package com.example.credibanco

import android.app.Application
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.credibanco.di.dataSourceModule
import com.example.credibanco.di.repositoryModule
import com.example.credibanco.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class CredibancoApp : Application() {
//    companion object {
//        lateinit var prefs: Prefs
//    }
    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger()
            androidContext(this@CredibancoApp)
            modules(listOf(viewModelModule, repositoryModule, dataSourceModule))
        }

    }
}


//package com.example.credibanco
//
//import android.app.Application
//import androidx.appcompat.app.AppCompatActivity
//import android.os.Bundle
//
//class MainActivity : Application() {
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_main)
//    }
//}