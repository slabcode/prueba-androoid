package com.example.credibanco.api

import com.example.credibanco.utils.Constants.Companion.BASE_URL
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

interface RetrofitInstance {

    companion object {

        private val retrofit by lazy {
            val client = OkHttpClient.Builder().build()
            /**
             * Retrofit client
             */
            Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL)
                .client(client)
                .build()
        }

        /**
         * The instance
         */
        val api: CredibancoApi by lazy {
            retrofit.create(CredibancoApi::class.java)

        }
    }
}