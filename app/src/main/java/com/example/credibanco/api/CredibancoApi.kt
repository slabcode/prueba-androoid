package com.example.credibanco.api

import android.util.Base64
import com.example.credibanco.data.entities.annul.Annul
import com.example.credibanco.data.entities.annul.AnnulResponse
import com.example.credibanco.data.entities.auth.Auth
import com.example.credibanco.data.entities.auth.AuthResponse
import com.example.credibanco.utils.Constants.Companion.BASE_URL_ANNUL
import com.example.credibanco.utils.Constants.Companion.BASE_URL_AUTH
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.POST

interface CredibancoApi {

    @POST(BASE_URL_AUTH)
    suspend fun authorizeTransaction(
        @Header("Authorization") authorization: String,
        @Body authTransaction: Auth
    ):AuthResponse

    @POST(BASE_URL_ANNUL)
    suspend fun annulTransaction(
        @Header("Authorization") authorization: String,
        @Body annulTransaction: Annul
    ):AnnulResponse
}