package com.example.credibanco.presentation.annul

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.example.credibanco.data.entities.annul.Annul
import com.example.credibanco.domain.annul.AnnulRepo
import com.example.credibanco.utils.Resource
import kotlinx.coroutines.Dispatchers
import java.lang.Exception

class AnnulViewModel(private val annulRepo: AnnulRepo) : ViewModel(){

    fun annulTransaction(authorization: String, annulBody: Annul) = liveData(Dispatchers.IO) {
        emit(Resource.Loading())
        try {
            emit(Resource.Success(annulRepo.annulTransaction(authorization, annulBody)))
        }
        catch (error: Exception) {
            emit(Resource.Failure(error))
        }
    }
}