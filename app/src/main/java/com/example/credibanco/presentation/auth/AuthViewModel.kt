package com.example.credibanco.presentation.auth

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.example.credibanco.data.entities.auth.Auth
import com.example.credibanco.domain.auth.AuthRepo
import com.example.credibanco.utils.Resource
import kotlinx.coroutines.Dispatchers
import java.lang.Exception

class AuthViewModel(private val authRepo: AuthRepo) : ViewModel(){

    fun authTransaction(authorization: String, authBody: Auth) = liveData(Dispatchers.IO) {
        emit(Resource.Loading())
        try {
            emit(Resource.Success(authRepo.authorizeTransaction(authorization, authBody)))
        }
        catch (error: Exception) {
            emit(Resource.Failure(error))
        }
    }
}