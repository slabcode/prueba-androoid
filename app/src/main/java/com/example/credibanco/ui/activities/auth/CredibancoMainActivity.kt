package com.example.credibanco.ui.activities.auth

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.credibanco.databinding.ActivityMainBinding

class CredibancoMainActivity:AppCompatActivity() {

    lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

    }
}