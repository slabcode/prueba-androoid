package com.example.credibanco.ui.fragments.annul

import android.os.Bundle
import android.util.Base64
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.example.credibanco.R
import com.example.credibanco.data.entities.annul.Annul
import com.example.credibanco.data.entities.auth.Auth
import com.example.credibanco.databinding.FragmentAnnulBinding
import com.example.credibanco.databinding.FragmentAuthBinding
import com.example.credibanco.presentation.annul.AnnulViewModel
import com.example.credibanco.presentation.auth.AuthViewModel
import com.example.credibanco.utils.Resource
import com.example.credibanco.utils.snackError
import com.example.credibanco.utils.snackWarning
import com.google.android.material.snackbar.Snackbar
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class AnnulFragment: Fragment(R.layout.fragment_annul) {

    private val viewModel by sharedViewModel<AnnulViewModel>()

    lateinit var binding: FragmentAnnulBinding

    private var password: String = "000123000ABC"

    val data = password.toByteArray(charset("UTF-8"))
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding = FragmentAnnulBinding.bind(view)
        onClick()


    }

    private fun onClick(){
        binding.AnnulButton.setOnClickListener {
            authorizeTransaction()
        }
    }

    private fun authorizeTransaction() {
        println("Error: ${Base64.encodeToString(data, Base64.DEFAULT)}")
        viewModel.annulTransaction(
            authorization = "Basic ${"MDAwMTIzMDAwQUJD"}",
            annulBody = Annul(
                binding.ReceiptEditText.text.toString(),
                binding.RRNEditText.text.toString()
            )
        ).observe(viewLifecycleOwner, Observer { result ->
            when (result) {
                is Resource.Loading -> {
//                        showProgressBar()
                }
                is Resource.Success -> {
//                        hideProgressBar()
                    binding.root.snackWarning(
                        result.data.statusDescription,
                        Snackbar.LENGTH_LONG
                    )
//                    println("Error: ${result.data.receiptId}")
//                    println("Error: ${result.data.rrn}")



//                        setupAdapter(orders)
                }
                is Resource.Failure -> {
//                        hideProgressBar()
                    binding.root.snackError(
                        "Error: ${result.exception.message}",
                        Snackbar.LENGTH_LONG
                    )
                    println("Error: ${result.exception.message}")

                }
            }
        })
    }
}