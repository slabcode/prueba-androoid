package com.example.credibanco.ui.fragments.auth

import android.os.Bundle
import android.util.Base64
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.example.credibanco.R
import com.example.credibanco.data.entities.auth.Auth
import com.example.credibanco.databinding.FragmentAuthBinding
import com.example.credibanco.presentation.auth.AuthViewModel
import com.example.credibanco.utils.Resource
import com.example.credibanco.utils.snackError
import com.example.credibanco.utils.snackWarning
import com.google.android.material.snackbar.Snackbar
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class AuthFragment : Fragment(R.layout.fragment_auth) {

    private val viewModel by sharedViewModel<AuthViewModel>()

    lateinit var binding: FragmentAuthBinding

    private var password: String = "000123000ABC"
    lateinit var password1: String



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding = FragmentAuthBinding.bind(view)
        onClick()


    }

    private fun onClick(){
        binding.LoginButton.setOnClickListener {
            authorizeTransaction()
        }
    }

    private fun authorizeTransaction() {
//        println("Error: ${Base64.encodeToString(data, Base64.DEFAULT)}")
        password1 = binding.CommerceEditText.text.toString() + binding.TerminalEditText.text.toString()
        val data = password1.toByteArray(charset("UTF-8"))
        val auth = Base64.encodeToString(data, Base64.NO_WRAP)
        println("Error: $password1")
        viewModel.authTransaction(
            authorization = "Basic $auth",
            authBody = Auth(
                binding.IdEditText.text.toString(),
                binding.CommerceEditText.text.toString(),
                binding.TerminalEditText.text.toString(),
                binding.AmountEditText.text.toString(),
                binding.Card4EditText.text.toString(),
            )
        ).observe(viewLifecycleOwner, Observer { result ->
            when (result) {
                is Resource.Loading -> {
//                        showProgressBar()
                }
                is Resource.Success -> {
//                        hideProgressBar()
                    binding.root.snackWarning(
                        result.data.statusDescription,
                        Snackbar.LENGTH_LONG
                    )
                    println("Error1: ${result.data.receiptId}")
                    println("Error2: ${result.data.rrn}")



//                        setupAdapter(orders)
                }
                is Resource.Failure -> {
//                        hideProgressBar()
                    binding.root.snackError(
                        "Error: ${result.exception.message}",
                        Snackbar.LENGTH_LONG
                    )
                    println("Error: ${result.exception.message}")

                }
            }
        })
    }
}