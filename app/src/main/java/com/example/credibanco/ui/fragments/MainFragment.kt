package com.example.credibanco.ui.fragments

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.credibanco.R
import com.example.credibanco.databinding.FragmentAnnulBinding
import com.example.credibanco.databinding.FragmentSelectionBinding

class MainFragment:  Fragment(R.layout.fragment_selection) {

    lateinit var binding: FragmentSelectionBinding

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding = FragmentSelectionBinding.bind(view)

        goToAuth()
        goToAnnul()

    }

    private fun goToAuth(){
        binding.AuthorizationButton.setOnClickListener {
            val action =
                MainFragmentDirections.actionMainFragmentToAuthFragment3()
            findNavController().navigate(action)
        }
    }

    private fun goToAnnul(){
        binding.AnnulButton.setOnClickListener {
            val action =
                MainFragmentDirections.actionMainFragmentToAnnulFragment3()
            findNavController().navigate(action)
        }
    }
}