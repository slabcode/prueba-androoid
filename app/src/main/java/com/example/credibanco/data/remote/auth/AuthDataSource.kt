package com.example.credibanco.data.remote.auth

import com.example.credibanco.api.RetrofitInstance
import com.example.credibanco.data.entities.auth.Auth
import com.example.credibanco.data.entities.auth.AuthResponse

class AuthDataSource {
    suspend fun authTransaction(authorization: String, authBody: Auth) : AuthResponse = RetrofitInstance.api.authorizeTransaction(authorization, authBody)
}