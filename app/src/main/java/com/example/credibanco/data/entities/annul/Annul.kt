package com.example.credibanco.data.entities.annul

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Annul (
    val receiptId: String = "",
    val rrn: String = "",

) : Parcelable