package com.example.credibanco.data.entities.auth

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Auth (
    val id: String = "",
    val commerceCode: String = "",
    val terminalCode: String = "",
    val amount: String = "",
    val card: String = ""

) : Parcelable