package com.example.credibanco.data.remote.annul

import com.example.credibanco.api.RetrofitInstance
import com.example.credibanco.data.entities.annul.Annul
import com.example.credibanco.data.entities.annul.AnnulResponse

class AnnulDataSource {
    suspend fun annulTransaction(authorization:String, annulBody: Annul) : AnnulResponse = RetrofitInstance.api.annulTransaction(authorization, annulBody)
}