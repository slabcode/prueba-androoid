package com.example.credibanco.data.entities.annul

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class AnnulResponse (
    @SerializedName("statusCode")
    val statusCode: String = "",
    @SerializedName("statusDescription")
    val statusDescription: String = ""
) : Parcelable