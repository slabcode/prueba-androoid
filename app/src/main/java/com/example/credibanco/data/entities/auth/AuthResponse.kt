package com.example.credibanco.data.entities.auth

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize


@Parcelize
data class AuthResponse (
    val receiptId: String = "",
    val rrn: String = "",
    val statusCode: String = "",
    val statusDescription: String = ""
):Parcelable