package com.example.credibanco.domain.auth

import com.example.credibanco.data.entities.auth.Auth
import com.example.credibanco.data.entities.auth.AuthResponse

interface AuthRepo {
    suspend fun authorizeTransaction(authorization:String , authBody: Auth): AuthResponse
}