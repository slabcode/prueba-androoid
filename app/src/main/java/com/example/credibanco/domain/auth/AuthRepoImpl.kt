package com.example.credibanco.domain.auth

import com.example.credibanco.data.entities.auth.Auth
import com.example.credibanco.data.entities.auth.AuthResponse
import com.example.credibanco.data.remote.auth.AuthDataSource

class AuthRepoImpl(private val  authDataSource: AuthDataSource) : AuthRepo {
    override suspend fun authorizeTransaction(authorization: String, authBody: Auth):AuthResponse =authDataSource.authTransaction(authorization, authBody)
}