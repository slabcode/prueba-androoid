package com.example.credibanco.domain.annul

import com.example.credibanco.data.entities.annul.Annul
import com.example.credibanco.data.entities.annul.AnnulResponse

interface AnnulRepo {

    suspend fun annulTransaction(authorization:String, annulBody: Annul): AnnulResponse
}