package com.example.credibanco.domain.annul

import com.example.credibanco.data.entities.annul.Annul
import com.example.credibanco.data.entities.annul.AnnulResponse
import com.example.credibanco.data.remote.annul.AnnulDataSource

class AnnulRepoImpl(private val annulDataSource: AnnulDataSource): AnnulRepo {
    override suspend fun annulTransaction(authorization: String, annulBody: Annul): AnnulResponse = annulDataSource.annulTransaction(authorization, annulBody)
}