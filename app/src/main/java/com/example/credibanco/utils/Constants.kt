package com.example.credibanco.utils

class Constants {
    companion object{
        const val BASE_URL = "http://localhost:8080/api/payments/"
        const val BASE_URL_AUTH = "${BASE_URL}authorization"
        const val BASE_URL_ANNUL = "${BASE_URL}annulation"
    }
}