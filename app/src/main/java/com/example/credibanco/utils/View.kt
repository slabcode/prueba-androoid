package com.example.credibanco.utils

import android.app.Dialog
import android.content.res.ColorStateList
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.Button
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.content.ContextCompat
import com.example.credibanco.R
import com.google.android.material.snackbar.Snackbar


/**
 * To hide a view
 */
fun View.hide() {
    this.visibility = View.GONE
}

/**
 * To show a view
 */
fun View.show() {
    this.visibility = View.VISIBLE
}

/**
 * To display a SnackBar
 */
fun View.snackError(message: String, duration: Int = Snackbar.LENGTH_LONG) {
    Snackbar.make(this, message, duration).setBackgroundTint(resources.getColor(R.color.error_snackbar)).show()
}

/**
 *To display an error snackbar
 */
fun View.snackWarning(message: String, duration: Int = Snackbar.LENGTH_LONG) {
    Snackbar.make(this, message, duration).setBackgroundTint(resources.getColor(R.color.green_title)).show()
}
/**
 * To display a snackBar with a predefined message
 */

fun showBasicError(view: View) {
    view.snackError("Ha ocurrido un error en el mapa")
}

/**
 * To display a snackBar with a button
 */

fun View.snackWithButton(
    message: String,
    duration: Int = Snackbar.LENGTH_LONG,
    acceptButton: () -> Unit
) {
    Snackbar.make(this, message, duration).setAction(resources.getString(R.string.accept)) {
        acceptButton()
    }.show()
}



/**
 * To change the style of a button
 */
fun Button.changeToButtonSelected() {
    this.apply {
        setBackgroundResource(R.drawable.button_background)
        backgroundTintList = ColorStateList.valueOf(resources.getColor(R.color.green_title))
        setTextColor(resources.getColor(R.color.white_text))
    }
}

fun Button.changeToButtonUnselected() {
    this.apply {
        setBackgroundResource(R.drawable.rounded_corners_layout)
        backgroundTintList = ColorStateList.valueOf(resources.getColor(R.color.white_text))
        setTextColor(resources.getColor(R.color.green_title))
    }
}