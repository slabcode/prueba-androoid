package com.example.credibanco.di

import com.example.credibanco.data.remote.annul.AnnulDataSource
import com.example.credibanco.data.remote.auth.AuthDataSource
import org.koin.dsl.module

val dataSourceModule = module {


    single { AuthDataSource() }

    single { AnnulDataSource() }


}