package com.example.credibanco.di

import com.example.credibanco.domain.annul.AnnulRepo
import com.example.credibanco.domain.annul.AnnulRepoImpl
import com.example.credibanco.domain.auth.AuthRepo
import com.example.credibanco.domain.auth.AuthRepoImpl
import org.koin.dsl.module

val repositoryModule = module {

    single<AuthRepo> { AuthRepoImpl(get()) }

    single<AnnulRepo> { AnnulRepoImpl(get()) }

}