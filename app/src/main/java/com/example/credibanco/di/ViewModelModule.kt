package com.example.credibanco.di

import com.example.credibanco.presentation.annul.AnnulViewModel
import com.example.credibanco.presentation.auth.AuthViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {


    viewModel { AuthViewModel(get()) }

    viewModel { AnnulViewModel(get()) }


}